account = IMAP {
    server = 'imap.free.fr',
    username = os.getenv('FREE_USERNAME'),
    password = os.getenv('FREE_PASSWORD'),
    port = 993,
    ssl = 'auto'
}

function get_state(fullstate)
    local state, category = "", ""

    if fullstate == nil then
        return state, category
    end

    idx = string.find(fullstate, ":")

    if idx == nil then
        state = fullstate
        category = ""
    elseif idx > 0 then
        state = string.sub(fullstate, 0, idx-1)
        category = string.sub(fullstate, idx+1)
    end

    return state, category
end


function filter_spam()
    spam = Set {}
    social = Set {}
    commercial = Set {}

    results = account.INBOX:is_unseen()

    for _, msg in ipairs(results) do
        mbox, uid = table.unpack(msg)
        spamcause = mbox[uid]:fetch_field("X-ProXaD-SC")
        print(os.date("%c") .. " headers >> " .. spamcause)
        fullstate, score = string.match(spamcause, "state=([a-zA-Z0-9 :]+) score=(%d+)")
        score = tonumber(score) or 0
        state, category = get_state(fullstate)

        print(os.date("%c") .. " state(" .. state .. ") category(" .. category .. ") score(" .. score .. ")")

        if state == "HAM" and (category == "CommercialEmailGeneric" or category == "CommercialEmailKnown")  then
            table.insert(commercial, msg)
        elseif state == "HAM" and category == "SocialNetwork" then
            table.insert(social, msg)
        elseif (state == "SPAM" or state == "HAM") and score > 75 then
            table.insert(spam, msg)
        end

    end
    commercial:move_messages(account.Junk)
    social:move_messages(account.Social)
    spam:move_messages(account.Trash)
end

function main()
    repeat
        filter_spam()
    until ifsys.sleep(60) ~= 0
end

main()
