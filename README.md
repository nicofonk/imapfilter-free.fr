# imapfilter on @free.fr domain

Free.fr has an anti-spam solution but the mailbox is not classified all emails remains in the INBOX.
That project provides rules to clean your mailbox with grey and spam emails. `imapfilter` tool and
classifications rules are packaged in a docker container.

## build

    docker build -t imapfilter-free.fr .

## run

    docker run --rm -it -e FREE_USERNAME=$FREE_USERNAME -e FREE_PASSWORD=$FREE_PASSWORD imapfilter-free.fr
