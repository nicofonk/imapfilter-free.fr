FROM alpine:3.14.2 as builder
ARG version=v2.7.5
ARG archive=$version.tar.gz

ADD https://github.com/lefcha/imapfilter/archive/$archive /ws/
RUN apk add build-base openssl-dev lua-dev pcre2-dev \
 && tar xvf /ws/$archive -C /ws/ \
 && cd /ws/imapfilter-${version#v} \
 && make install DESTDIR=/install

FROM alpine:3.14.2

COPY --from=builder /install /

RUN apk add pcre2 lua openssl \
 && addgroup -S imapfilter \
 && adduser -S imapfilter -G imapfilter
USER imapfilter
COPY --chown=imapfilter:imapfilter config.lua /home/imapfilter/.imapfilter/
RUN openssl s_client -connect free.fr:443 </dev/null 2>/dev/null|openssl x509 -outform PEM > /home/imapfilter/.imapfilter/certificates

ENTRYPOINT ["imapfilter"]
